# LiveCaption 

#### 介绍
LiveCaption 使用Golang开发，基于 [`GoVcl-GUI库`](https://gitee.com/ying32/govcl)。

LiveCaption 是一款支持使用第三方公共云接口，实时将系统/麦克风的声音识别显示为桌面字幕的 Windows 软件，支持其他语种的同步翻译。

LiveCaption 目前使用了以下第三方接口：
- 阿里云 [实时语音识别](https://ai.aliyun.com/nls/trans?spm=5176.7933691.h2v3icoap.220.77572a66602GJl)
- 百度翻译开放平台 [翻译API](http://api.fanyi.baidu.com/api/trans/product/index) 
- 腾讯云 [翻译API](https://cloud.tencent.com/product/tmt) 

软件帮助文档/使用教程看这个：[https://www.yuque.com/viggo-t7cdi/livecaption](https://www.yuque.com/viggo-t7cdi/livecaption)

软件配置教程：[https://www.bilibili.com/video/BV11i4y177mq?from=search&seid=17850534336494724311](https://www.bilibili.com/video/BV11i4y177mq?from=search&seid=17850534336494724311)


#### 界面预览
![主界面](https://ftp.bmp.ovh/imgs/2020/09/1ebc0500d7f4fa00.png)
![演示1](https://ftp.bmp.ovh/imgs/2020/09/eb31a61c56bc2f6d.png)
![演示2](https://ftp.bmp.ovh/imgs/2020/09/5051ce01ecfda2d1.png)
![演示3](https://ftp.bmp.ovh/imgs/2020/09/22aa550417f22dbb.png)
![演示4](https://ftp.bmp.ovh/imgs/2020/09/f3dbee1688a610bd.png)
![软件设置](https://ftp.bmp.ovh/imgs/2020/09/ede056423a43718a.png)

<a name="Download"></a>
#### Download

<a name="e66a66f1"></a>
##### 下载地址:
- (v1.0.8)  [点我下载](http://file.viggo.site/livecaption/1.0.8/livecaption-windows-1.0.8-x64.zip)

你也可以到 [release](https://gitee.com/641453620/livecaption/releases) 页面下载其他版本

<a name="1bbbb204"></a>
#### 注意事项

- 软件目录下的 `data` 目录为数据存储目录，请勿删除，否则可能会导致软件配置丢失。

#### 使用说明
- 视频教程，由B站UP `铁柱213` 提供：https://www.bilibili.com/video/BV11i4y177mq?p=2


#### 更新步骤
- 第一种：可以将新安装包中的 exe 复制到旧版本中，完成升级。
- 第二种：将旧版本的 data 目录复制到 新版本的软件目录下，直接覆盖，完成升级。


#### 交流&联系&反馈
    * QQ交流群：1002456027
    
    
<a name="AyJ3E"></a>
#### 捐赠&支持

![](https://pic2.superbed.cn/item/5e00b93476085c3289dd2dc0.png)